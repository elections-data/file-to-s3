package fr.open.data.map.filetos3;

import lombok.Data;

@Data
public class FileEntry {
    private String id;
    private String url;
    private String fileName;
    private String type;
    private S3 s3;

    @Data
    public static class S3 {
        private String bucket;
        private String path;
        private Long pushedAt;
    }
}
