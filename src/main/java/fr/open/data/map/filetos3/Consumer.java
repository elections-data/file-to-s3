package fr.open.data.map.filetos3;
import co.elastic.apm.api.CaptureTransaction;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

@RequiredArgsConstructor
@EnableBinding(Sink.class)
public class Consumer {

    private static Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    private final S3Client s3Client;

    @CaptureTransaction
    @StreamListener(Sink.INPUT)
    public void transferToS3(FileEntry fileEntry) throws IOException {
        MDC.put("document_id", fileEntry.getId());
        LOGGER.info("Handling Transfer");
        URLConnection fileUrlConnection = new URL(fileEntry.getUrl()).openConnection();
        PutObjectResponse response = s3Client.putObject(PutObjectRequest.builder()
                        .bucket(fileEntry.getS3().getBucket())
                        .key(fileEntry.getFileName())
                        .build(), RequestBody.fromInputStream(
                new BufferedInputStream(fileUrlConnection.getInputStream()),
                fileUrlConnection.getContentLength()
                )
        );
        fileEntry.getS3().setPushedAt(new Date().getTime());
        fileEntry.getS3().setPath(response.getValueForField("key", String.class).toString());
        LOGGER.info("Transfer done");
    }
}
