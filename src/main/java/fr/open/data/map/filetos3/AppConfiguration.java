package fr.open.data.map.filetos3;

import brave.sampler.Sampler;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

import java.net.URI;


@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "filetos3")
public class AppConfiguration {
    private s3 s3;

    @Data
    public static class s3 {
        private String region;
        private String endpoint;
    }

    @Bean
    public Sampler sampler() {
        return Sampler.ALWAYS_SAMPLE;
    }

    @Bean
    public S3Client s3Client() {
        Region s3Region = Region.of(s3.getRegion());
        return S3Client.builder()
                .region(s3Region)
                .endpointOverride(URI.create(s3.getEndpoint()))
                .build();
    }
}
